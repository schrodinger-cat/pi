var webpack = require('webpack');
var path = require('path');
var argv = require('yargs').argv;
var BowerWebpackPlugin = require("bower-webpack-plugin");

var plugins = [
    new webpack.NoErrorsPlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),
    new webpack.ResolverPlugin(
        new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin(".bower.json", ["main"])
    )
];

if (argv.production) {
    plugins.push(new webpack.optimize.UglifyJsPlugin({
        minimize: true,
        beautify: false,
        compress: true,
        comments: false
    }));
}

var config = {
    // context: __dirname,
    // entry: {
    //     index: './src/media/js/index'
    // },
    // output: {
    //     path: './build/media/js/',
    //     publicPath: '/media/js/',
    //     filename: '[name].js',
    // },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: [
                    /node_modules/,
                    /bower_components/
                ],
                loader: 'babel',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.json?$/,
                loader: 'json-loader',
            },
        ],
    },
    watch: argv.production ? false : true,
    resolve:  {
        modulesDirectories: ['node_modules', 'bower_components']
    },
    plugins: plugins,
    debug: argv.production ? false : true,
    devtool: argv.production ? null : '#cheap-inline-module-source-map',
    externals: {
        '../TweenLite': 'TweenLite',
        './TweenLite': 'TweenLite',
        'TweenLite': 'TweenLite',
        '../CSSPlugin': 'CSSPlugin',
        './CSSPlugin': 'CSSPlugin',
        'CSSPlugin': 'CSSPlugin',
        //'../plugins/CSSPlugin': 'CSSPlugin'
    }
};

module.exports = config;
