var App = global.App;

function Animations(){}

Animations.prototype = {
	init: function(){

		App.modules.Slides.onChange.add( this._animateSlideByIndex, this );
		this._animateSlideByIndex( 0 );
	},
	_animateSlideByIndex: function( index ){
		var method = this['_slideAnimation_' + index];
		method && method.apply(this);
	},

	_slideAnimation_0 : function(){
		TweenMax.fromTo(
			$('[data-start]'), 
			1, 
			{autoAlpha: 0, y: '-50%'}, 
			{autoAlpha: 1, y: '0%'}
		);		
	},
	_slideAnimation_1: function(){
		$('[data-about-clouds]').each( function() {
		    TweenMax.fromTo( 
		    	this , 15 + Math.random() * 4, 
		    	{x: -100}, 
		    	{x: 1000, ease: Linear.easeNone, repeat: -1, delay: 1 + Math.random()*10 })
		});
		TweenMax.fromTo( 
			$('[data-about-text]'), 1, 
			{ autoAlpha: 0, x: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 0.0 } 
		);
		TweenMax.fromTo( 
			$('[data-about-like]'), 1, 
			{ autoAlpha: 0, x: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 0.25 } 
		);
		TweenMax.fromTo( 
			$('[data-about-circle]'), 0.5, 
			{ autoAlpha: 0, y: '100%'}, 
			{ ease: Circ.easeOut, autoAlpha:1, y: '-50%', delay: 0.3 }
		);

		var pathObj = {
		    "about-arc": {
		        "strokepath": [
		            {
		                "path": "M155.4,28.7c15.4,16.1,24.8,37.9,24.8,62c0,49.6-40.2,89.8-89.8,89.8S0.6,140.3,0.6,90.7S40.8,0.9,90.4,0.9",
		                "duration": 600,
						"strokeColor": "#61ccaf"
		            },
		            {
		                "path": "M90.4,0.9c25.6,0,48.6,10.7,65,27.8",
		                "duration": 600,
						"strokeColor": "#e94c71"
		            }
		        ],
		        "dimensions": {
		            "width": 185,
		            "height": 185
		        }
		    }
		};

		setTimeout(function(){
			$('[data-about-path]').lazylinepainter( 
			{
				"svgData": pathObj,
				"strokeWidth": 2
			}).lazylinepainter('paint'); 
		}, 800);
		

	},
	_slideAnimation_2: function(){
		$('[data-culture-clouds]').each( function() {
		    TweenMax.fromTo( 
		    	this , 15 + Math.random() * 4, 
		    	{x: -100}, 
		    	{x: 1000, ease: Linear.easeNone, repeat: -1, delay: 1 + Math.random()*10 })
		});
		TweenMax.fromTo( 
			$('[data-culture-church]'), 1, 
			{ autoAlpha: 0, y: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, y: '0%', delay: 0 } 
		);
		TweenMax.fromTo( 
			$('[data-culture-positive]'), 1, 
			{ autoAlpha: 0, x: '-100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 0.5 } 
		);
		TweenMax.fromTo( 
			$('[data-culture-negative]'), 1, 
			{ autoAlpha: 0, x: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 0.75 } 
		);
		TweenMax.fromTo( 
			$('[data-culture-sub]'), 1, 
			{ autoAlpha: 0, x: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 1 } 
		);
 
		var pathObj = {
			"culture-arc": {
			    "strokepath": [
					{
						"path": "M45.2,28.1c34-26.8,83.3-20.9,110.1,13.1c9.6,12.2,15,26.4,16.4,40.8",
						"duration": 600,
						"strokeColor": "#e94c71"
					},
					{
						"path": "M140.1,170.1c-13.9,8.6-30.3,13.5-47.9,13.5c-50.5,0-91.5-41-91.5-91.5s41-91.5,91.5-91.5",
						"duration": 600,
						"strokeColor": "#61ccaf"
					}
				],
				"dimensions": {
				    "width": 173,
				    "height": 184
				}
			}
		};

		$('[data-culture-arc]').lazylinepainter( 
		{
			"svgData": pathObj,
			"strokeWidth": 2
		}).lazylinepainter('paint'); 

		TweenMax.fromTo( 
			$('[data-culture-like]'), 1, 
			{ autoAlpha: 0, x: '-100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 1.15 } 
		);
		TweenMax.fromTo( 
			$('[data-culture-dislike]'), 1, 
			{ autoAlpha: 0, x: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 1.15 } 
		);
		TweenMax.fromTo( 
			$('[data-culture-positive-list]'), 1, 
			{ autoAlpha: 0, x: '-100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 1.45 } 
		);
		TweenMax.fromTo( 
			$('[data-culture-negative-list]'), 1, 
			{ autoAlpha: 0, x: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 1.45 } 
		);
	},
	_slideAnimation_3: function(){
		$('[data-area-clouds]').each( function() {
		    TweenMax.fromTo( 
		    	this , 15 + Math.random() * 4, 
		    	{x: -100}, 
		    	{x: 1000, ease: Linear.easeNone, repeat: -1, delay: 1 + Math.random()*10 })
		});
		TweenMax.fromTo( 
			$('[data-area-house]'), 1, 
			{ autoAlpha: 0, x: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 0 } 
		);
		TweenMax.fromTo( 
			$('[data-area-car]'), 6, 
			{ x: -85 }, 
			{ ease: Power0.easeNone, x: 785, delay: 0.15, repeatDelay: 1 + Math.random() * 5 ,repeat: -1 } 
		);
		TweenMax.fromTo( 
			$('[data-area-total]'), 1, 
			{ autoAlpha: 0, x: '-100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 0.4 } 
		);
		TweenMax.fromTo( 
			$('[data-area-total]'), 1, 
			{ autoAlpha: 0, x: '-100%'}, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 0.4 } 
		);
 
		var pathObj = {
		    "area-arc": {
		        "strokepath": [
		            {
		                "path": "M257.8,232.4c-3.7,4.9-8.9,11-15.8,17.2c-3,2.7-11.2,9.7-23.5,16.2c-12.8,6.8-23.9,9.8-28.7,11   c-12,2.9-21.4,3.2-27,3.3c-7.9,0.2-20.2,0.4-35.3-3.7c-8.8-2.4-15.1-5.2-20.3-7.7c-7-3.2-18-8.4-29.7-18   c-3.1-2.6-15.7-13.2-26.7-31.3c-11.3-18.8-15.6-36.7-17.3-48.7",
		                "duration": 600,
						"strokeColor": "#e94c71"
		            },
		            {
		                "path": "M258.5,267.1c-5.7,5.2-10.8,9.3-14.9,12.3c-5.1,3.8-9.6,7.3-16.3,11c-6.2,3.5-11.1,5.5-16.5,7.8   c-4.6,1.9-10.1,4.2-17.3,6.3c-12.9,3.7-23.2,4.5-29.5,5c-12.9,0.9-22.8,0.1-26.8-0.3c-12.1-1.2-21.2-3.7-25.3-4.8   c-11.7-3.4-20.1-7.4-25.5-10c-5.2-2.5-12.8-6.2-21.8-12.7c-2.5-1.8-9.8-7.1-18.2-15.3c-5.3-5.3-13.4-14-21.2-26.2   c-8.1-12.6-12.4-23.4-14.2-28c-2.3-6.1-5.5-15.7-7.5-27.8c-1.3-7.5-1.6-13.3-1.8-16.8c-0.2-3.6-0.8-14.6,0.3-26.3   c2.2-22.4,10.2-39.5,13.7-46.8c8.6-18,18.6-30.3,23.8-36.2c6.2-6.9,17.2-18.2,33.7-28.3c14.2-8.8,26.5-13.2,31.5-14.8   c1.7-0.6,10.4-3.4,22.2-5.5c10.2-1.8,18-2.2,19.7-2.3c0,0,11.5-0.6,23.2,0.7c15.5,1.7,40.2,9.1,67.4,24.9",
		                "duration": 1400,
						"strokeColor": "#61ccaf"
		            }
		        ],
		        "dimensions": {
		            "width": 420,
		            "height": 471
		        }
		    }
		};

		$('[data-area-arc]').lazylinepainter( 
		{
			"svgData": pathObj,
			"strokeWidth": 2
		}).lazylinepainter('paint'); 


		TweenMax.fromTo( 
			$('[data-area-safe]'), 1, 
			{ autoAlpha: 0, x: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 1 } 
		);
		TweenMax.staggerFromTo( 
			$('[data-area-flower]').children(), 0.8, 
			{ autoAlpha: 0, scale: 0.5 }, 
			{ ease: Back.easeOut, autoAlpha: 1, scale: 1, delay: 1.1 },
			-0.2 
		);
		TweenMax.fromTo( 
			$('[data-area-unsafe]'), 1, 
			{ autoAlpha: 0, x: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 1 } 
		);
		TweenMax.staggerFromTo( 
			$('[data-area-gun]').children(), 0.8, 
			{ autoAlpha: 0, scale: 0.5 }, 
			{ ease: Back.easeOut, autoAlpha: 1, scale: 1, delay: 1.1 },
			-0.2 
		);
		TweenMax.fromTo( 
			$('[data-area-positive]'), 1, 
			{ autoAlpha: 1, x: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 1.5 } 
		);
		TweenMax.fromTo( 
			$('[data-area-negative]'), 1, 
			{ autoAlpha: 1, x: '-100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 1.5 } 
		);
	},
	_slideAnimation_4: function(){
		$('[data-sport-clouds]').each( function() {
		    TweenMax.fromTo( 
		    	this , 15 + Math.random() * 4, 
		    	{x: -100}, 
		    	{x: 1000, ease: Linear.easeNone, repeat: -1, delay: 1 + Math.random()*10 })
		});
		TweenMax.fromTo( 
			$('[data-sport-stadium]'), 1, 
			{ autoAlpha: 0, x: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 0 }
		);
		TweenMax.fromTo( 
			$('[data-sport-bench]'), 1, 
			{ autoAlpha: 0, x: '-100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 0.2 }
		);
		TweenMax.fromTo( 
			$('[data-sport-text]'), 1, 
			{ autoAlpha: 0, y: '-100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, y: '0%', delay: 0.4 }
		);
		TweenMax.fromTo( 
			$('[data-sport-list]'), 1, 
			{ autoAlpha: 0, x: '-100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 0.5 }
		);
		TweenMax.fromTo( 
			$('[data-sport-stat]'), 1, 
			{ autoAlpha: 0, }, 
			{ ease: Circ.easeOut, autoAlpha: 1, delay: 0.5 }
		);
		TweenMax.staggerFromTo( 
			$('[data-sport-balls]').children(), 0.35, 
			{ scale: 0.5, alpha: 0 }, 
			{ scale: 1, alpha: 1, ease:Back.easeOut, delay: 0.6}, 
			-0.1
		);
		TweenMax.fromTo( 
			$('[data-sport-positive]'), 1, 
			{ autoAlpha: 0, x: '-100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 0.8 } 
		);
		TweenMax.fromTo( 
			$('[data-sport-negative]'), 1, 
			{ autoAlpha: 0, x: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, x: '0%', delay: 0.8 } 
		);
	},

	_slideAnimation_5: function(){
		TweenMax.fromTo( 
			$('[data-end-info]'), 1, 
			{ autoAlpha: 0, y: '-100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, y: '0%', delay: 0 } 
		);
		TweenMax.fromTo( 
			$('[data-end-text]'), 1, 
			{ autoAlpha: 0, y: '100%' }, 
			{ ease: Circ.easeOut, autoAlpha: 1, y: '0%', delay: 0.2 } 
		);	
	}
}

module.exports = new Animations();