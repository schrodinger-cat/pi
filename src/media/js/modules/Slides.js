var App = global.App;

function Slides() {};

Slides.prototype = {
    init: function () {

        this.onChange = new App.classes.Callback();

        this.index = -1;
        this.$slides = $('[data-slide]').show();
        
        this.totalSlides = this.$slides.length;
        this.maxIndex = this.totalSlides - 1;

        this.LOCK_WHEEL_TIME = 600;
        this.collectedWheels = 0;
        this.wheelDirection = 0;

        TweenMax.set(this.$slides, {force3D: true})

        if(App.env.isMac){
            this.LOCK_WHEEL_TIME = 1200;
        }

        var self = this;

        this.$up = $('[data-slide-up]').click(function (e) {
            e.preventDefault();
            self.addSlideIndex(-1);
        });

        this.$down = $('[data-slide-down]').click(function (e) {
            e.preventDefault();
            self.addSlideIndex(1);
        });

        if (App.env.isBrowser) {
            App.dom.$window.on('DOMMouseScroll mousewheel', function (e) {
                if (!e) e = event;
                var o = e.originalEvent,
                    d = o.detail, w = o.wheelDelta,
                    n = 225, n1 = n - 1;

                d = d ? w && (f = w / d) ? d / f : -d / 1.35 : w / 120;
                d = d < 1 ? d < -1 ? (-Math.pow(d, 2) - n1) / n : d : (Math.pow(d, 2) + n1) / n;
                var delta = Math.min(Math.max(d / 2, -1), 1);
                delta = delta > 0 ? 1 : -1;

                self._testExtraWheel(delta);
            });
        } else {
            var hammer = new Hammer(App.dom.$body[0], {
                recognizers: [
                    [Hammer.Swipe, { direction: Hammer.DIRECTION_VERTICAL }],
                ],
            });
            hammer.on('swipe', function (e) {
                if (e.direction == 8) {
                    self._testExtraWheel(-1);
                } else if (e.direction == 16) {
                    self._testExtraWheel(1);
                }
            });
        }

        this.showSlide(0, true);
    },

    _testExtraWheel: function (delta) {
        if (this.locked) {
            return;
        }

        this.addSlideIndex(-delta);
    },

    addSlideIndex: function (direction) {
        this.showSlide(this.index + (direction > 0 ? 1 : -1));
    },

    _lock: function () {
        clearTimeout(this.lockTimeout);

        this.locked = true;
        var self = this;
        this.lockTimeout = setTimeout(function () {
            self.locked = false;
        }, this.LOCK_WHEEL_TIME);
    },

    showSlide: function (newIndex, immediate) {
        newIndex = newIndex < 0 ? 0 : newIndex;
        newIndex = newIndex > this.maxIndex ? this.maxIndex : newIndex;

        if (newIndex == this.index) {
            return;
        }

        this._lock();

        var direction = newIndex > this.index ? 1 : -1;

        this.index = newIndex;

        var $showSlide = this.$slides.eq(this.index);
        var $hideSlides = this.$slides.not($showSlide);

        $showSlide.css({ zIndex: 1 });
        $hideSlides.css({ zIndex: 0 });

        var tweenTime = immediate ? 0 : 0.5;

        TweenMax.fromTo($showSlide, tweenTime, { y: 100 * direction, autoAlpha: 0 }, { y: 0, autoAlpha: 1 });
        TweenMax.to($hideSlides, tweenTime, { y: -100 * direction, autoAlpha: 0 });

        var showUp = this.index > 0;
        var showDown = this.index < this.maxIndex;

        if (this.showUp != showUp) {
            this.showUp = showUp;
            if (showUp) {
                immediate ? this.$up.removeClass('no-pe').show() : this.$up.removeClass('no-pe').fadeIn();
            } else {
                immediate ? this.$up.addClass('no-pe').hide() : this.$up.addClass('no-pe').fadeOut();
            }
        }

        if (this.showDown != showDown) {
            this.showDown = showDown;
            if (showDown) {
                immediate ? this.$down.removeClass('no-pe').show() : this.$down.removeClass('no-pe').fadeIn();
            } else {
                immediate ? this.$down.addClass('no-pe').hide() : this.$down.addClass('no-pe').fadeOut();
            }
        }

        var $baloons = $('[data-baloon]').show();
        var $baloon = $baloons.filter('[data-baloon="phone"]');

        if(this.index == 1) {
            TweenMax.to($baloons, 0.1, {scale: 0, autoAlpha: 0});
            TweenMax.to($baloon, 0.5, {delay: 2, autoAlpha: 1, scale: 1, clearProps: 'transform'});
        }

        this.onChange.call(this.index);
    },
};

module.exports = new Slides();
