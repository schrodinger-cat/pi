function SharingManager(){};

SharingManager.prototype = {
    init: function () {

        var $share = $('[data-share]');

        var self = this;
        var title = 'Интерактивная инфографика Санкт-Петербург';
        var description = 'представлена ФОМ';
        
        $share.click(function(e) {
            e.preventDefault();

            var $this = $(this);
            var socialName = $this.attr('data-share');
            var imageUrl = $this.attr('data-share-image') || '';

            if (socialName == 'vk') {
                self.vkontakte(
                    'http://sborkaproject.com/demo/piter/',
                    title, 
                    imageUrl, 
                    description
                );

            } else if (socialName == 'fb') {
                self.facebook(
                    'http://sborkaproject.com/demo/piter/',
                    title, 
                    imageUrl, 
                    description
                );
            }
        });
    },

    vkontakte: function(purl, ptitle, pimg, text) {
        purl = purl || document.location.href;
        var url = 'http://vkontakte.ru/share.php?';
        url += 'url=' + encodeURIComponent(purl);
        url += '&title=' + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&image=' + encodeURIComponent(pimg);
        url += '&noparse=true';
        this.popup(url);

        //console.log("Share vk: " + purl + " / " + ptitle + " / " + pimg + " / " + text)
    },

    facebook: function(purl, ptitle, pimg, text) {
        purl = purl || document.location.href;
        var url = 'https://www.facebook.com/dialog/feed?';
        url += 'app_id=627231160786748';
        url += '&display=popup';
        url += '&caption=' + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&link=' + encodeURIComponent(purl);
        url += '&redirect_uri' + encodeURIComponent(purl);
        url += '&picture=' + encodeURIComponent(pimg);

        this.popup(url);

        //console.log("Share fb: " + purl + " / " + ptitle + " / " + pimg + " / " + text)
    },

    popup: function(url) {
        window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
    }
};

module.exports = new SharingManager();