var $ = global.$ = global.jQuery = require('./vendor/jquery.min.js');

var TweenMax = global.TweenMax = require('./vendor/tweenmax.min.js');
								 require('./vendor/jquery.gsap.min.js');

require('./vendor/jquery.helpers.js');
require('./vendor/hammer.min.js');
require('./vendor/lazypainter.min.js');

var App = global.App = new (function App(){

	// Base DOM structure
	this.dom = {
		$body: $('body'),
		$html: $('html'),
		$document: $(document),
		$window: $(window)
	};

	// SVG Sprites
	function addSVGSprite( data ){
        $('<div style="width:0;height:0;overflow:hidden"></div>')
        	.prependTo(document.body)
        	.html(typeof XMLSerializer != 'undefined'
        		? (new XMLSerializer()).serializeToString(data.documentElement)
        		: $(data.documentElement).html()
        	);
	}
    $.get('media/svg/sprite.svg', addSVGSprite);

	// Environment settings
	var MobileDetect = require('./vendor/mobile-detect.min.js');
	var mobileDetectInstance = new MobileDetect(window.navigator.userAgent);

	this.env = {
		isMobile: !!mobileDetectInstance.mobile(),
		isTablet: !!mobileDetectInstance.tablet(),
		isPhone: !!mobileDetectInstance.phone(),
		isBrowser: !(!!mobileDetectInstance.mobile()),
		isMac: navigator.platform.indexOf('Mac') > -1,
		isWin: navigator.platform.indexOf('Win') > -1,
		detector: mobileDetectInstance,
	};

	this.env.isMobile	&& this.dom.$html.addClass('_mobile');
	this.env.isTablet	&& this.dom.$html.addClass('_tablet');
	this.env.isPhone	&& this.dom.$html.addClass('_phone');
	this.env.isBrowser	&& this.dom.$html.addClass('_browser');
	this.env.isMac		&& this.dom.$html.addClass('_mac');
	this.env.isWin		&& this.dom.$html.addClass('_win');

	// Classes
	this.classes = {};

	// Modules
	this.modules = {};

	// Helpers
	this.helpers = {};

	// Utils
	this.utils = {
        now: function(){
            var P = 'performance';
            if (window[P] && window[P]['now']) {
                this.now = function(){ return window.performance.now() }
            } else {
                this.now = function(){ return +(new Date()) }
            }
            return this.now();
        }
	}

	// Startup
	TweenMax.CSSPlugin.defaultTransformPerspective = 300;

	var self = this;
	$(function(){

		// Module init order is important!
		self.modules.Slides.init();
		self.modules.SharingManager.init();
		self.modules.Animations.init();

		// Remove _loading modificator
		self.dom.$html.removeClass('_loading');
	});

})();

// import classes first
App.classes.Callback		= require('./classes/Callback');

// import modules
App.modules.Slides 			= require('./modules/Slides');
App.modules.SharingManager 	= require('./modules/SharingManager');
App.modules.Animations 		= require('./modules/Animations');

global.Piter = global.App, delete global.App;